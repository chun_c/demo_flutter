//import flutter helper library
import 'dart:convert';

import 'package:demo_flutter_app/src/testscreen.dart';
import 'package:demo_flutter_app/widgets/image_list.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' show get;
import 'package:demo_flutter_app/models/image_model.dart';

class App extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return AppState();
  }

}

//Create a class that will be our custom widget
//This class must extend the 'StatelessWidget' base class
class AppState extends State<App>{
  int counter = 0;
  List<ImageModel> images = [];

  void fetchImage() async {
    counter ++;
    var response =  await get('https://jsonplaceholder.typicode.com/photos/$counter');
    var imageModel = ImageModel.fromJson(json.decode(response.body));

    setState(() {
      images.add(imageModel);
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: ImageList(images),
        floatingActionButton: FloatingActionButton(
          child:Icon(Icons.image),
          onPressed: (){
            fetchImage();
//            setState(() {
//              counter+=1;
//            });
       //   gotoSecondActivity(context);
          },
        ),
        appBar:AppBar(
          title: Text('Lets see some images!'),
        ),
      ),
    );
  }

  gotoSecondActivity(BuildContext context){

    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => TestScreen()),
    );

  }

}

