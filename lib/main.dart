//Need to import helper library to get content on the screen
import 'package:flutter/material.dart';
import 'src/app.dart';

//Define a 'main' function to run when our app start
void main() {
  //Create a new text widget to show some text on the screen
  //Take that widget and get it on the screen
  runApp(MaterialApp(
    home: App() ,
  ),
  );}
